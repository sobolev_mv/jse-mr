package ru.nlmk.sobolevmv.tm.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;
import ru.nlmk.sobolevmv.tm.entity.Project;
import ru.nlmk.sobolevmv.tm.exception.ProjectNotFoundException;
import ru.nlmk.sobolevmv.tm.exception.TaskNotFoundException;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

class ProjectServiceTest {

  public static final String PROJECT_NAME1 = "Project1";
  public static final String PROJECT_NAME2 = "Project2";
  public static final String DESC = "Description1";
  public static final long USER_ID = 1234567L;
  public static final long PROJECT_ID1 = 7654321L;
  public static final long PROJECT_ID2 = 8654321L;
  public static final long PROJECT_ID_NONEXISTS = 222333L;
  public static final String PROJECT_NAME_NONEXISTS = "QQQ";
  private ProjectService projectService;

  @BeforeEach
  private void setup(){
    projectService = ProjectService.getInstance();
    projectService.clear();
  }

  @Test
  void testCreate1() {
    Project projectResult = projectService.create(PROJECT_NAME1);
    projectResult.setId(PROJECT_ID1);
    Project projectExp = new Project(PROJECT_NAME1);
    projectExp.setId(PROJECT_ID1);
    assertTrue(new ReflectionEquals(projectExp).matches(projectResult));
  }

  @Test
  void testCreate2() {
    Project projectResult = projectService.create(PROJECT_NAME1, DESC);
    projectResult.setId(PROJECT_ID1);
    Project projectExp = new Project(PROJECT_NAME1, DESC);
    projectExp.setId(PROJECT_ID1);
    assertTrue(new ReflectionEquals(projectExp).matches(projectResult));
  }

  @Test
  void testCreate3() {
    Project projectResult = projectService.create(PROJECT_NAME1, DESC, USER_ID);
    projectResult.setId(PROJECT_ID1);
    Project projectExp = new Project(PROJECT_NAME1, DESC, USER_ID);
    projectExp.setId(PROJECT_ID1);
    assertTrue(new ReflectionEquals(projectExp).matches(projectResult));
  }

  @Test
  void testCreate1EmptyNull() {
    Project projectResult = projectService.create("");
    assertNull(projectResult);
    projectResult = projectService.create(null);
    assertNull(projectResult);
  }

  @Test
  void testCreate2EmptyNull() {
    Project projectResult = projectService.create("", "");
    assertNull(projectResult);
    projectResult = projectService.create(null, null);
    assertNull(projectResult);
  }

  @Test
  void testUpdate() throws ProjectNotFoundException {
    Project projectResult = projectService.create(PROJECT_NAME1,"",USER_ID);
    projectResult.setId(PROJECT_ID1);
    projectService.update(PROJECT_ID1, PROJECT_NAME1, DESC, USER_ID);
    Project projectExp = new Project(PROJECT_NAME1, DESC, USER_ID);
    projectExp.setId(PROJECT_ID1);
    assertTrue(new ReflectionEquals(projectExp).matches(projectResult));
  }

  @Test
  void testUpdateException() throws ProjectNotFoundException {
    Project projectResult = projectService.create(PROJECT_NAME1,"",USER_ID);
    projectResult.setId(PROJECT_ID1);
    assertThrows(ProjectNotFoundException.class, ()-> projectService.update(999788L, PROJECT_NAME1, DESC, USER_ID));
  }

  @Test
  void testFindByIndex() throws ProjectNotFoundException, TaskNotFoundException {
    Project projectResult = projectService.create(PROJECT_NAME1,"",USER_ID);
    projectResult.setId(PROJECT_ID1);
    Project projectExp = projectService.findByIndex(0, USER_ID);
    assertTrue(new ReflectionEquals(projectExp).matches(projectResult));
  }

  @Test
  void testFindByIndexException() throws ProjectNotFoundException, TaskNotFoundException {
    Project projectResult = projectService.create(PROJECT_NAME1,"",USER_ID);
    projectResult.setId(PROJECT_ID1);
    assertThrows(ProjectNotFoundException.class, ()-> projectService.findByIndex(9, USER_ID));
  }

  @Test
  void testFindByName() throws ProjectNotFoundException {
    List<Project> lstProjectResult = new ArrayList<>();
    Project projectResult = projectService.create(PROJECT_NAME1,"",USER_ID);
    projectResult.setId(PROJECT_ID1);
    lstProjectResult.add(projectResult);
    List<Project> lstProjectExp = projectService.findByName(PROJECT_NAME1, USER_ID);
    assertTrue(new ReflectionEquals(lstProjectExp.get(0)).matches(lstProjectResult.get(0)));
  }

  @Test
  void testFindByNameException() throws ProjectNotFoundException {
    List<Project> lstProjectResult = new ArrayList<>();
    Project projectResult = projectService.create(PROJECT_NAME1,"",USER_ID);
    projectResult.setId(PROJECT_ID1);
    lstProjectResult.add(projectResult);
    assertThrows(ProjectNotFoundException.class, ()-> projectService.findByName(PROJECT_NAME_NONEXISTS, USER_ID));
  }

  @Test
  void testFindById() throws ProjectNotFoundException {
    Project projectResult = projectService.create(PROJECT_NAME1,"",USER_ID);
    projectResult.setId(PROJECT_ID1);
    Project projectExp = projectService.findById(PROJECT_ID1, USER_ID);
    assertTrue(new ReflectionEquals(projectExp).matches(projectResult));
  }

  @Test
  void testFindByIdException() {
    Project projectResult = projectService.create(PROJECT_NAME1,"",USER_ID);
    projectResult.setId(PROJECT_ID1);
    assertThrows(ProjectNotFoundException.class, ()-> projectService.findById(PROJECT_ID_NONEXISTS, USER_ID));
  }

  @Test
  void testRemoveByIndex() throws TaskNotFoundException, ProjectNotFoundException {
    Project projectResult = projectService.create(PROJECT_NAME1,"",USER_ID);
    projectResult.setId(PROJECT_ID1);
    projectResult = projectService.create(PROJECT_NAME1,"",USER_ID);
    projectResult.setId(PROJECT_ID2);
    Project projectExp = projectService.removeByIndex(1, USER_ID);
    assertTrue(new ReflectionEquals(projectExp).matches(projectResult));
  }

  @Test
  void testRemoveByIndexException() throws TaskNotFoundException, ProjectNotFoundException {
    Project projectResult = projectService.create(PROJECT_NAME1,"",USER_ID);
    projectResult.setId(PROJECT_ID1);
    assertThrows(ProjectNotFoundException.class, ()-> projectService.removeByIndex(1, USER_ID));
  }

  @Test
  void testRemoveById() throws ProjectNotFoundException {
    Project projectResult = projectService.create(PROJECT_NAME1,"",USER_ID);
    projectResult.setId(PROJECT_ID1);
    projectResult = projectService.create(PROJECT_NAME1,"",USER_ID);
    projectResult.setId(PROJECT_ID2);
    Project projectExp = projectService.removeById(PROJECT_ID2, USER_ID);
    assertTrue(new ReflectionEquals(projectExp).matches(projectResult));
  }

  @Test
  void testRemoveByIdException() throws ProjectNotFoundException {
    Project projectResult = projectService.create(PROJECT_NAME1,"",USER_ID);
    projectResult.setId(PROJECT_ID1);
    assertThrows(ProjectNotFoundException.class, ()-> projectService.removeById(PROJECT_ID_NONEXISTS, USER_ID));
  }

  @Test
  void testRemoveByName() throws ProjectNotFoundException {
    Project projectResult = projectService.create(PROJECT_NAME1,"",USER_ID);
    projectResult.setId(PROJECT_ID1);
    projectResult = projectService.create(PROJECT_NAME2,"",USER_ID);
    projectResult.setId(PROJECT_ID2);
    List<Project> lstProjectResult = projectService.removeByName(PROJECT_NAME2, USER_ID);

    List<Project> lstProjectExp = new ArrayList<>();
    Project projectExp = new Project(PROJECT_NAME2,"",USER_ID);
    projectExp.setId(PROJECT_ID2);
    lstProjectExp.add(projectExp);

    assertTrue(new ReflectionEquals(lstProjectExp.get(0)).matches(lstProjectResult.get(0)));
  }

  @Test
  void testRemoveByNameException() throws ProjectNotFoundException {
    Project projectResult = projectService.create(PROJECT_NAME1,"",USER_ID);
    projectResult.setId(PROJECT_ID1);
    projectResult = projectService.create(PROJECT_NAME2,"",USER_ID);
    projectResult.setId(PROJECT_ID2);
    assertThrows(ProjectNotFoundException.class, ()-> projectService.removeByName(PROJECT_NAME_NONEXISTS, USER_ID));
  }

  @Test
  void testFindAll() {
    Project projectResult = projectService.create(PROJECT_NAME1,"",null);
    projectResult.setId(PROJECT_ID1);
    projectResult = projectService.create(PROJECT_NAME2,"",USER_ID);
    projectResult.setId(PROJECT_ID2);

    List<Project> lstProjectExp = new ArrayList<>();
    Project projectExp = new Project(PROJECT_NAME1,"",null);
    projectExp.setId(PROJECT_ID1);
    lstProjectExp.add(projectExp);
    projectExp = new Project(PROJECT_NAME2,"",USER_ID);
    projectExp.setId(PROJECT_ID2);
    lstProjectExp.add(projectExp);
    assertTrue(new ReflectionEquals(lstProjectExp.get(0)).matches(projectService.findAll().get(0)));
    assertTrue(new ReflectionEquals(lstProjectExp.get(1)).matches(projectService.findAll().get(1)));
  }

  @Test
  void testFindAllByUserId() {
    Project projectResult = projectService.create(PROJECT_NAME2,"",USER_ID);
    projectResult.setId(PROJECT_ID2);

    List<Project> lstProjectExp = new ArrayList<>();
    Project projectExp = new Project(PROJECT_NAME1,"",null);
    projectExp.setId(PROJECT_ID1);
    lstProjectExp.add(projectExp);
    projectExp = new Project(PROJECT_NAME2,"",USER_ID);
    projectExp.setId(PROJECT_ID2);
    lstProjectExp.add(projectExp);
    assertTrue(new ReflectionEquals(lstProjectExp.get(1)).matches(projectService.findAllByUserId(USER_ID).get(0)));
  }

}