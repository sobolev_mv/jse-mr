package ru.nlmk.sobolevmv.tm;

import ru.nlmk.sobolevmv.tm.enumerated.Role;
import ru.nlmk.sobolevmv.tm.observer.Listener.ListenerProjectImpl;
import ru.nlmk.sobolevmv.tm.observer.Listener.ListenerSystemImpl;
import ru.nlmk.sobolevmv.tm.observer.Listener.ListenerTaskImpl;
import ru.nlmk.sobolevmv.tm.observer.Listener.ListenerUserImpl;
import ru.nlmk.sobolevmv.tm.observer.Publisher.Publisher;
import ru.nlmk.sobolevmv.tm.observer.Publisher.PublisherImpl;
import ru.nlmk.sobolevmv.tm.service.*;

public class Application {

    static {
        UserService.getInstance().create("admin", "111", "Admin", "Adminov", Role.ADMIN);
        UserService.getInstance().create("user", "222", "User", "Userov", Role.USER);
        ProjectService.getInstance().create("Project1", "", UserService.getInstance().findByLogin("admin").getId());
        ProjectService.getInstance().create("Project2", "", UserService.getInstance().findByLogin("admin").getId());
        ProjectService.getInstance().create("Project3", "", UserService.getInstance().findByLogin("admin").getId());
        TaskService.getInstance().create("Task1", "", UserService.getInstance().findByLogin("admin").getId());
        TaskService.getInstance().create("Task1", "desc", UserService.getInstance().findByLogin("user").getId());
        TaskService.getInstance().create("Task1", "", UserService.getInstance().findByLogin("admin").getId());
        TaskService.getInstance().create("Task2", "", UserService.getInstance().findByLogin("admin").getId());
        TaskService.getInstance().create("Task1", "", UserService.getInstance().findByLogin("admin").getId());
        TaskService.getInstance().create("NTask", "", UserService.getInstance().findByLogin("admin").getId());
    }

    public static void main(final String[] args) {
        SystemService.getInstance().displayWelcome();
        Publisher publisher = new PublisherImpl();
        ListenerUserImpl listenerUser = new ListenerUserImpl();
        ListenerProjectImpl listenerProject = new  ListenerProjectImpl();
        ListenerTaskImpl listenerTask = new  ListenerTaskImpl();
        ListenerSystemImpl listenerSystem = new  ListenerSystemImpl();
        publisher.addListener(listenerUser);
        publisher.addListener(listenerProject);
        publisher.addListener(listenerTask);
        publisher.addListener(listenerSystem);
        publisher.start();
    }


    public ProjectService getProjectService() {
        return ProjectService.getInstance();
    }

    public TaskService getTaskService() {
        return TaskService.getInstance();
    }

    public ProjectTaskService getProjectTaskService() {
        return ProjectTaskService.getInstance();
    }

    public UserService getUserService() {
        return UserService.getInstance();
    }
}
