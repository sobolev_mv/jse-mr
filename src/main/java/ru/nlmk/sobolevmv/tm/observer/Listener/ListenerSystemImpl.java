package ru.nlmk.sobolevmv.tm.observer.Listener;

import ru.nlmk.sobolevmv.tm.service.SystemService;

import static ru.nlmk.sobolevmv.tm.constant.TerminalConst.*;

public class ListenerSystemImpl implements Listener {
    private final SystemService systemService = SystemService.getInstance();

    @Override
    public int update(String param) {
        switch (param) {
            case HISTORY:
                return systemService.displayHistory();
            case VERSION:
                return systemService.displayVersion();
            case ABOUT:
                return systemService.displayAbout();
            case HELP:
                return systemService.displayHelp();
            case EXIT:
                return systemService.displayExit();
            default:
                return -1;
        }
    }
}
