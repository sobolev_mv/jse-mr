package ru.nlmk.sobolevmv.tm.observer.Listener;

import lombok.SneakyThrows;
import ru.nlmk.sobolevmv.tm.exception.ProjectNotFoundException;
import ru.nlmk.sobolevmv.tm.exception.TaskNotFoundException;
import ru.nlmk.sobolevmv.tm.service.ProjectService;

import static ru.nlmk.sobolevmv.tm.constant.TerminalConst.*;

public class ListenerProjectImpl implements Listener {
    private final ProjectService projectService = ProjectService.getInstance();

    @SneakyThrows
    @Override
    public int update(String param) throws TaskNotFoundException, ProjectNotFoundException {
        switch (param) {
            case PROJECT_CREATE:
                return projectService.createProject();
            case PROJECT_CLEAR:
                return projectService.clearProject();
            case PROJECT_LIST:
                return projectService.listProject();
            case PROJECT_VIEW_BY_INDEX:
                return projectService.viewProjectByIndex();
            case PROJECT_VIEW_BY_ID:
                return projectService.viewProjectById();
            case PROJECT_VIEW_BY_NAME:
                return projectService.viewProjectByName();
            case PROJECT_REMOVE_BY_INDEX:
                return projectService.removeProjectByIndex();
            case PROJECT_REMOVE_BY_ID:
                return projectService.removeProjectById();
            case PROJECT_REMOVE_BY_NAME:
                return projectService.removeProjectByName();
            case PROJECT_UPDATE_BY_INDEX:
                return projectService.updateProjectByIndex();
            case PROJECT_UPDATE_BY_ID:
                return projectService.updateProjectById();
            case PROJECTS_TO_FILE_XML:
                return projectService.saveXML(PROJECTS_FILE_NAME_XML);
            case PROJECTS_TO_FILE_JSON:
                return projectService.saveJSON(PROJECTS_FILE_NAME_JSON);
            case PROJECTS_FROM_FILE_XML:
                return projectService.uploadFromXML(PROJECTS_FILE_NAME_XML);
            case PROJECTS_FROM_FILE_JSON:
                return projectService.uploadFromJSON(PROJECTS_FILE_NAME_JSON);
            default:
                return -1;
        }
    }
}
