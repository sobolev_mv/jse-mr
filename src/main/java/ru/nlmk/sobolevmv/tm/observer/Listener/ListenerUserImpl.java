package ru.nlmk.sobolevmv.tm.observer.Listener;

import ru.nlmk.sobolevmv.tm.service.UserService;

import static ru.nlmk.sobolevmv.tm.constant.TerminalConst.*;

public class ListenerUserImpl implements Listener {
    private final UserService userService = UserService.getInstance();

    @Override
    public int update(String param) {
        switch (param) {
            case LOG_ON:
                return userService.registry();
            case LOG_OFF:
                return userService.logOff();
            case USER_INFO:
                return userService.displayUserInfo();
            case USER_UPDATE:
                return userService.updateUser();
            case USER_UPDATE_PASSWORD:
                return userService.updatePassword();
            case USER_CREATE:
                return userService.createUser();
            case USER_CLEAR:
                return userService.clearUser();
            case USER_LIST:
                return userService.listUser();
            case USER_VIEW_BY_LOGIN:
                return userService.viewUserByLogin();
            case USER_UPDATE_BY_LOGIN:
                return userService.updateUserByLogin();
            case USER_REMOVE_BY_LOGIN:
                return userService.removeUserByLogin();
            case USERS_TO_FILE_XML:
                return userService.saveXML(USERS_FILE_NAME_XML);
            case USERS_TO_FILE_JSON:
                return userService.saveJSON(USERS_FILE_NAME_JSON);
            case USERS_FROM_FILE_XML:
                return userService.uploadFromXML(USERS_FILE_NAME_XML);
            case USERS_FROM_FILE_JSON:
                return userService.uploadFromJSON(USERS_FILE_NAME_JSON);
            default:
                return -1;
        }
    }
}
