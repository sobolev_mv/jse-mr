package ru.nlmk.sobolevmv.tm.observer.Publisher;

import ru.nlmk.sobolevmv.tm.exception.ProjectNotFoundException;
import ru.nlmk.sobolevmv.tm.exception.TaskNotFoundException;
import ru.nlmk.sobolevmv.tm.observer.Listener.Listener;

public interface Publisher {
    void addListener(Listener listener);
    void deleteListener(Listener listener);
    void notify(String command) throws TaskNotFoundException, ProjectNotFoundException;

    void start();
}
