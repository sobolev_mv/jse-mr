package ru.nlmk.sobolevmv.tm.observer.Listener;

import ru.nlmk.sobolevmv.tm.exception.ProjectNotFoundException;
import ru.nlmk.sobolevmv.tm.exception.TaskNotFoundException;

public interface Listener {
    int update(String command) throws TaskNotFoundException, ProjectNotFoundException;
}
