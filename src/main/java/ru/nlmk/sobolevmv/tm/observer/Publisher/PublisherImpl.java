package ru.nlmk.sobolevmv.tm.observer.Publisher;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nlmk.sobolevmv.tm.exception.ProjectNotFoundException;
import ru.nlmk.sobolevmv.tm.exception.TaskNotFoundException;
import ru.nlmk.sobolevmv.tm.observer.Listener.Listener;
import ru.nlmk.sobolevmv.tm.repository.ProjectRepository;
import ru.nlmk.sobolevmv.tm.service.SystemService;
import ru.nlmk.sobolevmv.tm.service.UserService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static ru.nlmk.sobolevmv.tm.constant.TerminalConst.ALL_ACTIONS;
import static ru.nlmk.sobolevmv.tm.constant.TerminalConst.EXIT;

public class PublisherImpl implements Publisher {
    List<Listener> listeners = new ArrayList<>();
    private final UserService userService = UserService.getInstance();
    private final SystemService systemService = SystemService.getInstance();
    public static final Logger logger = LogManager.getLogger(ProjectRepository.class);

    @Override
    public void start() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            if (command == null || command.isEmpty()) continue;
            if (!Arrays.asList(ALL_ACTIONS).contains(command)) {
                if (userService.checkAuthorisation(command) == -1) continue;
            }
            systemService.addCommandToHistory(command);
            try {
                notify(command);
            } catch (ProjectNotFoundException | TaskNotFoundException exception ) {
                logger.error(exception.getMessage());
            }
            System.out.println();
        }
    }

    @Override
    public void addListener(Listener listener) {
        if (!listeners.contains(listener)){
            listeners.add(listener);
        }
    }

    @Override
    public void deleteListener(Listener listener) {
        listeners.remove(listener);
    }

    @Override
    public void notify(String command) throws TaskNotFoundException,ProjectNotFoundException {
        for (Listener listener : listeners){
            listener.update(command);
        }

    }
}
