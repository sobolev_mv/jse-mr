package ru.nlmk.sobolevmv.tm.observer.Listener;

import lombok.SneakyThrows;
import ru.nlmk.sobolevmv.tm.service.TaskService;

import static ru.nlmk.sobolevmv.tm.constant.TerminalConst.*;

public class ListenerTaskImpl implements Listener {
    private final TaskService taskService = TaskService.getInstance();

    @SneakyThrows
    @Override
    public int update(String param)  {
        switch (param) {
            case TASK_CREATE:
                return taskService.createTask();
            case TASK_CLEAR:
                return taskService.clearTask();
            case TASK_LIST:
                return taskService.listTask();
            case TASK_VIEW_BY_INDEX:
                return taskService.viewTaskByIndex();
            case TASK_VIEW_BY_ID:
                return taskService.viewTaskById();
            case TASK_VIEW_BY_NAME:
                return taskService.viewTaskByName();
            case TASK_REMOVE_BY_INDEX:
                return taskService.removeTaskByIndex();
            case TASK_REMOVE_BY_ID:
                return taskService.removeTaskById();
            case TASK_REMOVE_BY_NAME:
                return taskService.removeTaskByName();
            case TASK_UPDATE_BY_INDEX:
                return taskService.updateTaskByIndex();
            case TASK_UPDATE_BY_ID:
                return taskService.updateTaskById();
            case TASK_LIST_BY_PROJECT_ID:
                return taskService.listTaskByProjectId();
            case TASK_ADD_TO_PROJECT_BY_IDS:
                return taskService.addTaskToProjectByIds();
            case TASK_REMOVE_FROM_PROJECT_BY_IDS:
                return taskService.removeTaskFromProjectByIds();
            case TASKS_TO_FILE_XML:
                return taskService.saveXML(TASKS_FILE_NAME_XML);
            case TASKS_TO_FILE_JSON:
                return taskService.saveJSON(TASKS_FILE_NAME_JSON);
            case TASKS_FROM_FILE_XML:
                return taskService.uploadFromXML(TASKS_FILE_NAME_XML);
            case TASKS_FROM_FILE_JSON:
                return taskService.uploadFromJSON(TASKS_FILE_NAME_JSON);
            default:
                return -1;
        }
    }
}

