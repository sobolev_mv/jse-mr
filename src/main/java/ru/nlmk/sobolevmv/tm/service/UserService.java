package ru.nlmk.sobolevmv.tm.service;

import ru.nlmk.sobolevmv.tm.entity.User;
import ru.nlmk.sobolevmv.tm.enumerated.Role;
import ru.nlmk.sobolevmv.tm.repository.UserRepository;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static ru.nlmk.sobolevmv.tm.util.HashUtil.hashMD5;

public class UserService extends AbstractService  {

    private final UserRepository userRepository;

    public User currentUser;

    private static volatile UserService instance = null;

    private UserService() {
        this.userRepository = UserRepository.getInstance();
    }

    public static UserService getInstance(){
        if (instance == null ) {
            synchronized (UserService.class) {
                if (instance == null) {
                    instance = new UserService();
                }
            }
        }
        return instance;
    }

    public User create(
            String login, String password,
            String firstName, String lastName, Role role)
    {
        if (login == null) return null;
        if (password == null) return null;
        if (firstName == null) return null;
        if (lastName == null) return null;
        if (role == null) return null;
        return userRepository.create(login, hashMD5(password), firstName, lastName, role);
    }

    public User create(Long id, String login, String password, Role role) {
        if (id == null) return null;
        if (login == null) return null;
        if (password == null) return null;
        if (role == null) return null;
        return userRepository.create(login, hashMD5(password), role);
    }

    public boolean checkPassword(final User user, final String password) {
        return user.getPassword().equals(hashMD5(password));
    }

    public User findByLogin(String login) {
        if (login == null) return null;
        return userRepository.findByLogin(login);
    }

    public User findById(Long id) {
        if (id == null) return null;
        return userRepository.findById(id);
    }

    public User updateByLogin(String login, String password, String firstName, String lastName) {
        if (login == null) return null;
        if (password == null) return null;
        if (firstName == null) return null;
        if (lastName == null) return null;
        return userRepository.updateByLogin(login, hashMD5(password), firstName, lastName);
    }

    public User updateById(Long id, String password, String firstName, String lastName) {
        if (id == null) return null;
        if (password == null) return null;
        if (firstName == null) return null;
        if (lastName == null) return null;
        return userRepository.updateById(id, hashMD5(password), firstName, lastName);
    }

    public User updatePasswordByLogin(String login, String password) {
        if (login == null) return null;
        if (password == null) return null;
        return userRepository.updatePasswordByLogin(login, hashMD5(password));
    }

    public User updatePasswordById(Long id, String password) {
        if (id == null) return null;
        if (password == null) return null;
        return userRepository.updatePasswordById(id, hashMD5(password));
    }

    public User removeByLogin(String login) {
        if (login == null) return null;
        return userRepository.removeByLogin(login);
    }

    public User removeById(Long id) {
        if (id == null) return null;
        return userRepository.removeById(id);
    }

    public void clear() {
        userRepository.clear();
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public int createUser() {
        System.out.println("[PLEASE, ENTER LOGIN:]");
        final String login = scanner.nextLine();
        System.out.println("[PLEASE, ENTER password:]");
        final String password = scanner.nextLine();
        System.out.println("[PLEASE, ENTER FIRST NAME:]");
        final String firstName = scanner.nextLine();
        System.out.println("[PLEASE, ENTER LAST NAME:]");
        final String lastName = scanner.nextLine();

        if (create(login, password, firstName, lastName, Role.USER) == null) {
            logger.info("Choose different login.");
        } else {
            logger.info("User is created.");
        }
        return 0;
    }

    public int updateUserByLogin() {
        System.out.println("[UPDATE USER]");
        System.out.println("[PLEASE, ENTER LOGIN:]");
        final String login = scanner.nextLine();
        final User user = findByLogin(login);
        if (user == null) {
            logger.info("User is not found.");
        } else {
            System.out.println("[PLEASE, ENTER PASSWORD:]");
            final String password = scanner.nextLine();
            System.out.println("[PLEASE, ENTER FIRST NAME:]");
            final String firstName = scanner.nextLine();
            System.out.println("[PLEASE, ENTER LAST NAME:]");
            final String lastName = scanner.nextLine();
            updateByLogin(login, password, firstName, lastName);
            logger.info("User is updated.");
        }
        return 0;
    }

    public int updateUserById() {
        System.out.println("[UPDATE USER]");
        System.out.println("[PLEASE, ENTER USER ID:]");
        final User user = findById(inputIdCheckFormat());
        if (user == null) {
            logger.info("User is not found.");
        } else {
            System.out.println("[PLEASE, ENTER PASSWORD:]");
            final String password = scanner.nextLine();
            System.out.println("[PLEASE, ENTER FIRST NAME:]");
            final String firstName = scanner.nextLine();
            System.out.println("[PLEASE, ENTER LAST NAME:]");
            final String lastName = scanner.nextLine();
            updateById(user.getId(), password, firstName, lastName);
            logger.info("User is updated.");
        }
        return 0;
    }

    public int updatePasswordById() {
        System.out.println("[UPDATE USER]");
        System.out.println("[PLEASE, ENTER USER ID:]");
        final User user = findById(inputIdCheckFormat());
        if (user == null) {
            logger.info("User is not found.");
        } else {
            System.out.println("[PLEASE, ENTER PASSWORD:]");
            final String password = scanner.nextLine();
            updatePasswordById(user.getId(), password);
            logger.info("User is updated.");
        }
        return 0;
    }

    public int updatePasswordByLogin() {
        System.out.println("[UPDATE USER]");
        System.out.println("[PLEASE, ENTER LOGIN:]");
        final String login = scanner.nextLine();
        final User user = findByLogin(login);
        if (user == null) {
            logger.info("User is not found.");
        } else {
            System.out.println("[PLEASE, ENTER PASSWORD:]");
            final String password = scanner.nextLine();
            updatePasswordByLogin(login, password);
            logger.info("Password is updated.");
        }
        return 0;
    }

    public int clearUser() {
        System.out.println("[CLEAR USER]");
        clear();
        logger.info("User was deleted.");
        return 0;
    }

    public int removeUserByLogin() {
        System.out.println("[CLEAR USER BY LOGIN]");
        System.out.println("ENTER LOGIN: ");
        final String login = scanner.nextLine();
        final User user = removeByLogin(login);
        if (user == null)
            logger.info("User was not deleted by login.");
        else
            logger.info("User was deleted by login.");
        return 0;
    }

    public int removeUserById() {
        System.out.println("[CLEAR USER BY ID]");
        System.out.println("ENTER ID: ");
        final User user = removeById(inputIdCheckFormat());
        if (user == null)
            logger.info("User was not deleted by id.");
        else
            logger.info("User was deleted by id.");
        return 0;
    }

    public void viewUser(final User user) {
        if (user == null) return;
        System.out.println("[VIEW USER]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("PASSWORD HASH: " + user.getPassword());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
        System.out.println("[OK]");
    }

    public void viewUsers(final List<User> users) {
        if (users == null || users.isEmpty()) {
            logger.info("User are not found.");
            return;
        }
        int index = 1;
        for (final User user : users) {
            System.out.println(index + ". " + user.getId() + ": " + user.getLogin());
            index++;
        }
        System.out.println("[OK]");
    }

    public int listUser() {
        System.out.println("[LIST USER]");
        viewUsers(findAll());
        return 0;
    }

    public int viewUserByLogin() {
        System.out.println("ENTER LOGIN: ");
        final User user = findByLogin(scanner.nextLine());
        viewUser(user);
        return 0;
    }

    public int displayUserInfo() {
        viewUser(instance.currentUser);
        return 0;
    }

    public int registry() {
        System.out.println("ENTER LOGIN: ");
        final User user = findByLogin(scanner.nextLine());
        if (user == null) {
            System.out.println("LOGIN IS NOT FOUND IN SYSTEM.");
            return 0;
        }
        System.out.println("ENTER PASSWORD: ");
        if (checkPassword(user, scanner.nextLine())) {
            System.out.println("WELCOME, " + user.getLogin());
            instance.currentUser = user;
        } else {
            System.out.println("FAIL");
        }
        return 0;
    }

    public int checkAuthorisation(final String param) {
        if (!Arrays.asList(Role.ADMIN.getActions()).contains(param)) {
            System.out.println("[FAIL]");
            logger.info("WRONG OPERATION.");
            return -1;
        }
        if (instance.currentUser == null) {
            System.out.println("PLEASE, LOG ON OR REGISTRY.");
            return -1;
        }
        if (!Arrays.asList(instance.currentUser.getRole().getActions()).contains(param)) {
            System.out.println("NOT ALLOWED.");
            return -1;
        }
        return 0;
    }

    public int logOff() {
        instance.currentUser = null;
        System.out.println("LOG OFF");
        return 0;
    }

    public int updatePassword() {
        System.out.println("[UPDATE PASSWORD OF CURRENT USER]");
        if (instance.currentUser == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[PLEASE, ENTER OLD PASSWORD:]");
            final String oldPassword = scanner.nextLine();
            if (checkPassword(instance.currentUser, oldPassword)) {
                System.out.println("[PLEASE, ENTER NEW PASSWORD:]");
                final String newPassword = scanner.nextLine();
                updatePasswordByLogin(instance.currentUser.getLogin(), newPassword);
                System.out.println("[OK]");
            } else {
                System.out.println("[WRONG PASSWORD]");
            }
        }
        return 0;
    }

    public int updateUser() {
        if (instance.currentUser == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[PLEASE, ENTER PASSWORD:]");
            final String password = scanner.nextLine();
            System.out.println("[PLEASE, ENTER FIRST NAME:]");
            final String firstName = scanner.nextLine();
            System.out.println("[PLEASE, ENTER LAST NAME:]");
            final String lastName = scanner.nextLine();
            updateById(instance.currentUser.getId(), password, firstName, lastName);
            System.out.println("[OK]");
        }
        return 0;
    }

    public int saveJSON(final String  fileName){
        if (fileName == null) return -1;
        try {
            userRepository.saveJSON(fileName);
        } catch (IOException e) {
            return sayErrorInFiles(e.getMessage());
        }
        System.out.println("[OK]");
        return 0;
    }

    public int saveXML(final String fileName){
        if (fileName == null) return -1;
        try {
            userRepository.saveXML(fileName);
        } catch (IOException e) {
            return sayErrorInFiles(e.getMessage());
        }
        System.out.println("[OK]");
        return 0;
    }

    public int uploadFromJSON(final String  fileName){
        if (fileName == null) return -1;
        try {
            userRepository.uploadJSON(fileName);
        } catch (IOException e) {
            return sayErrorInFiles(e.getMessage());
        }
        System.out.println("[OK]");
        return 0;
    }

    public int uploadFromXML(final String  fileName){
        if (fileName == null) return -1;
        try {
            userRepository.uploadXML(fileName);
        } catch (IOException e) {
            return sayErrorInFiles(e.getMessage());
        }
        System.out.println("[OK]");
        return 0;
    }


}
