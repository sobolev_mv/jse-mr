package ru.nlmk.sobolevmv.tm.service;

import ru.nlmk.sobolevmv.tm.entity.Task;
import ru.nlmk.sobolevmv.tm.exception.ProjectNotFoundException;
import ru.nlmk.sobolevmv.tm.exception.TaskNotFoundException;
import ru.nlmk.sobolevmv.tm.repository.TaskRepository;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Pattern;


public class TaskService extends AbstractService  {

    private final TaskRepository taskRepository;

    private static volatile TaskService instance = null;

    private final ProjectTaskService projectTaskService = ProjectTaskService.getInstance();

    private final UserService userService = UserService.getInstance();


    private TaskService() {
        this.taskRepository = TaskRepository.getInstance();
    }

    public static TaskService getInstance(){
        if (instance == null ) {
            synchronized (TaskService.class) {
                if (instance == null) {
                    instance = new TaskService();
                }
            }
        }
        return instance;
    }

    public Task create(final String name) {
        if (name == null) return null;
        return taskRepository.create(name);
    }

    public Task create(final String name, final String description) {
        if (name == null) return null;
        return taskRepository.create(name, description);
    }

    public Task create(final String name, final String description, final Long userId) {
        if (name == null) return null;
        return taskRepository.create(name, description, userId);
    }

    public Task update(final Long id, final String name, final String description, final Long userId) throws TaskNotFoundException {
        if (id == null) return null;
        if (name == null) return null;
        return taskRepository.update(id, name, description,userId);
    }

    public Task findByIndex(final int index,final Long userId) throws TaskNotFoundException {
        return taskRepository.findByIndex(index,userId);
    }

    public List<Task> findByName(final String name,final Long userId) throws TaskNotFoundException {
        if (name == null) return null;
        return taskRepository.findByName(name,userId);
    }

    public Task findById(final Long id, final Long userId) throws TaskNotFoundException {
        if (id == null) return null;
        return taskRepository.findById(id,userId);
    }

    public Task removeByIndex(final int index,final Long userId) throws TaskNotFoundException {

        return taskRepository.removeByIndex(index,userId);
    }

    public Task removeById(final Long id, final Long userId) throws TaskNotFoundException {
        if (id == null) return null;
        return taskRepository.removeById(id,userId);
    }

    public List<Task> removeByName(final String name,final Long userId) throws TaskNotFoundException {
        if (name == null) return null;
        return taskRepository.removeByName(name,userId);
    }

    public void clear() {
        taskRepository.clear();
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        if (projectId == null) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }

    public Task findByProjectIdAndId(final Long projectId, final Long id) {
        if (projectId == null) return null;
        if (id == null) return null;
        return taskRepository.findByProjectIdAndId(projectId, id);
    }

    public List<Task> findAllByUserId(final Long userId) {
        if (userId == null) return null;
        return taskRepository.findAllByUserId(userId);
    }

    public int createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("[PLEASE, ENTER TASK NAME:]");
        final String name = scanner.nextLine();
        if(Pattern.compile("^\\d").matcher(name).lookingAt()){
            System.out.println("[NAMES SHOULD NOT START FROM NUMBERS]");
            return -1;
        }
        System.out.println("[PLEASE, ENTER TASK DESCRIPTION:]");
        final String description = scanner.nextLine();
        if (userService.currentUser == null) create(name, description);
        else create(name, description, userService.currentUser.getId());
        System.out.println("[OK]");
        return 0;
    }

    public int updateTaskByIndex() throws TaskNotFoundException {
        System.out.println("[UPDATE TASK]");
        System.out.println("[PLEASE, ENTER TASK INDEX:]");
        final Long userId = userService.currentUser.getId();
        final Task task = findByIndex(inputIndexCheckFormat(),userId);
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[PLEASE, ENTER TASK NAME:]");
            final String name = scanner.nextLine();
            System.out.println("[PLEASE, ENTER TASK DESCRIPTION:]");
            final String description = scanner.nextLine();
            update(task.getId(), name, description,userId);
            System.out.println("[OK]");
        }
        return 0;
    }

    public int updateTaskById() throws TaskNotFoundException {
        System.out.println("[UPDATE TASK]");
        System.out.println("[PLEASE, ENTER TASK ID:]");
        final Long userId = userService.currentUser.getId();
        final Task task = findById(inputIdCheckFormat(),userId);
        if (task == null) {
            System.out.println("[FAIL]");
            return 0;
        } else {
            System.out.println("[PLEASE, ENTER TASK NAME:]");
            final String name = scanner.nextLine();
            System.out.println("[PLEASE, ENTER TASK DESCRIPTION:]");
            final String description = scanner.nextLine();
            update(task.getId(), name, description,userId);
            System.out.println("[OK]");
            return 0;
        }
    }

    public int clearTask() throws TaskNotFoundException {
        System.out.println("[CLEAR TASK]");
        if (userService.currentUser == null) {
            clear();
            projectTaskService.clear();
        } else {
            for (Task task : findAllByUserId(userService.currentUser.getId())) {
                removeById(task.getId(),userService.currentUser.getId());
            }
        }
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskByName() throws TaskNotFoundException {
        System.out.println("[CLEAR TASK BY NAME]");
        System.out.println("ENTER TASK NAME: ");
        final String name = scanner.nextLine();
        final List<Task> task = removeByName(name,userService.currentUser.getId());
        if (task == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");
        return 0;
    }

    public int removeTaskByIndex() throws TaskNotFoundException {
        System.out.println("[CLEAR TASK BY INDEX]");
        System.out.println("ENTER TASK INDEX: ");
        final Task task = removeByIndex(inputIndexCheckFormat(),userService.currentUser.getId());
        if (task == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");
        return 0;
    }

    public int removeTaskById() throws TaskNotFoundException {
        System.out.println("[CLEAR TASK BY ID]");
        System.out.println("ENTER TASK ID: ");
        final Task task = removeById(inputIdCheckFormat(),userService.currentUser.getId());
        if (task == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");
        return 0;
    }

    public void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
    }

    public int viewTaskByIndex() throws TaskNotFoundException {
        System.out.println("ENTER TASK INDEX: ");
        final Task task = findByIndex(inputIndexCheckFormat(),userService.currentUser.getId());
        viewTask(task);
        return 0;
    }

    public int viewTaskById() throws TaskNotFoundException {
        System.out.println("ENTER TASK ID: ");
        final Task task = findById(inputIdCheckFormat(),userService.currentUser.getId());
        viewTask(task);
        return 0;
    }

    public int viewTaskByName() throws TaskNotFoundException {
        System.out.println("ENTER TASK NAME: ");
        final List<Task> tasks = findByName(scanner.nextLine(),userService.currentUser.getId());
        viewTasks(tasks);
        return 0;
    }

    public void viewTasks(final List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) {
            System.out.println("[TASKS ARE NOT FOUND]");
            return;
        }
        int index = 1;
        tasks.sort(Comparator.comparing(Task::getName));
        for (final Task task : tasks) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
        System.out.println("[OK]");
    }

    public int listTask() {
        System.out.println("[LIST TASK]");
        List<Task> taskList;
        if (userService.currentUser == null) taskList = findAll();
        else taskList = findAllByUserId(userService.currentUser.getId());
        viewTasks(taskList);
        return 0;
    }

    public int listTaskByProjectId() {
        System.out.println("[LIST TASKS BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID: ");
        final long projectId = inputIdCheckFormat();
        final List<Task> tasks = findAllByProjectId(projectId);
        viewTasks(tasks);
        return 0;
    }

    public int addTaskToProjectByIds() throws ProjectNotFoundException, TaskNotFoundException {
        System.out.println("[ADD TASK TO PROJECT BY IDS]");
        System.out.println("ENTER PROJECT ID: ");
        final long projectId = inputIdCheckFormat();
        System.out.println("ENTER TASK ID: ");
        final long taskId = inputIdCheckFormat();
        projectTaskService.addTaskToProject(projectId, taskId,userService.currentUser.getId());
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskFromProjectByIds() {
        System.out.println("[REMOVE TASK FROM PROJECT BY IDS]");
        System.out.println("ENTER PROJECT ID: ");
        final long projectId = inputIdCheckFormat();
        System.out.println("ENTER TASK ID: ");
        final long taskId = inputIdCheckFormat();
        projectTaskService.removeTaskFromProject(projectId, taskId);
        System.out.println("[OK]");
        return 0;
    }


    public int saveJSON(final String  fileName){
        if (fileName == null) return -1;
        try {
            taskRepository.saveJSON(fileName);
        } catch (IOException e) {
            return sayErrorInFiles(e.getMessage());
        }
        System.out.println("[OK]");
        return 0;
    }

    public int saveXML(final String fileName){
        if (fileName == null) return -1;
        try {
            taskRepository.saveXML(fileName);
        } catch (IOException e) {
            return sayErrorInFiles(e.getMessage());
        }
        System.out.println("[OK]");
        return 0;
    }

    public int uploadFromJSON(final String  fileName){
        if (fileName == null) return -1;
        try {
            taskRepository. uploadJSON(fileName,Task.class);
        } catch (IOException e) {
            return sayErrorInFiles(e.getMessage());
        }
        System.out.println("[OK]");
        return 0;
    }

    public int uploadFromXML(final String  fileName){
        if (fileName == null) return -1;
        try {
            taskRepository. uploadXML(fileName, Task.class);
        } catch (IOException e) {
            return sayErrorInFiles(e.getMessage());
        }
        System.out.println("[OK]");
        return 0;
    }


}
