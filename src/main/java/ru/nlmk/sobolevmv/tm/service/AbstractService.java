package ru.nlmk.sobolevmv.tm.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public abstract class AbstractService {
    protected final Scanner scanner = new Scanner(System.in);

    protected final Logger logger = LogManager.getLogger(AbstractService.class);

    public int inputIndexCheckFormat() {
        final int index;
        try {
            index = Integer.parseInt(scanner.nextLine()) - 1;
        } catch (Throwable t) {
            System.out.println("[WRONG FORMAT]");
            return -1;
        }
        return index;
    }

    public long inputIdCheckFormat() {
        final long id;
        try {
            id = Long.parseLong(scanner.nextLine());
        } catch (Throwable t) {
            System.out.println("[WRONG FORMAT]");
            return 0L;
        }
        return id;
    }

    public int sayErrorInFiles(String message){
        System.out.println("[FAIL]");
        logger.error(message);
        return -1;
    }

}
