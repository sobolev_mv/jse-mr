package ru.nlmk.sobolevmv.tm.service;

import ru.nlmk.sobolevmv.tm.entity.Project;
import ru.nlmk.sobolevmv.tm.entity.Task;
import ru.nlmk.sobolevmv.tm.exception.ProjectNotFoundException;
import ru.nlmk.sobolevmv.tm.exception.TaskNotFoundException;
import ru.nlmk.sobolevmv.tm.repository.ProjectRepository;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Pattern;

public class ProjectService extends AbstractService {

    private static volatile ProjectService instance = null;

    private final ProjectRepository projectRepository;

    private final ProjectTaskService projectTaskService = ProjectTaskService.getInstance();

    private final UserService userService = UserService.getInstance();

    private ProjectService() {
        this.projectRepository = ProjectRepository.getInstance();
    }

    public static ProjectService getInstance() {
        if (instance == null) {
            synchronized (ProjectService.class) {
                if (instance == null) {
                    instance = new ProjectService();
                }
            }
        }
        return instance;
    }

    public Project create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name);
    }

    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name, description);
    }

    public Project create(final String name, final String description, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name, description, userId);
    }

    public Project update(final Long id, final String name, final String description, final Long userId) throws ProjectNotFoundException {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        return projectRepository.update(id, name, description, userId);
    }

    public Project findByIndex(final int index, final Long userId) throws TaskNotFoundException, ProjectNotFoundException {
        return projectRepository.findByIndex(index, userId);
    }

    public List<Project> findByName(final String name, final Long userId) throws ProjectNotFoundException {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByName(name, userId);
    }

    public Project findById(final Long id, final Long userId) throws ProjectNotFoundException {
        if (id == null) return null;
        return projectRepository.findById(id, userId);
    }

    public Project removeByIndex(final int index, final Long userId) throws ProjectNotFoundException, TaskNotFoundException {
        return projectRepository.removeByIndex(index, userId);
    }

    public Project removeById(final Long id, final Long userId) throws ProjectNotFoundException {
        if (id == null) return null;
        return projectRepository.removeById(id, userId);
    }

    public List<Project> removeByName(final String name, final Long userId) throws ProjectNotFoundException {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeByName(name, userId);
    }

    public void clear() {
        projectRepository.clear();
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public List<Project> findAllByUserId(Long userId) {
        if (userId == null) return null;
        return projectRepository.findAllByUserId(userId);
    }

    public int createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("[PLEASE, ENTER PROJECT NAME:]");
        final String name = scanner.nextLine();
        if(Pattern.compile("^\\d").matcher(name).lookingAt()){
            System.out.println("[NAMES SHOULD NOT START FROM NUMBERS]");
            return -1;
        }
        System.out.println("[PLEASE, ENTER PROJECT DESCRIPTION:]");
        final String description = scanner.nextLine();
        if (userService.currentUser == null) create(name, description);
        else create(name, description, userService.currentUser.getId());
        return 0;
    }

    public int updateProjectByIndex() throws ProjectNotFoundException, TaskNotFoundException {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[PLEASE, ENTER PROJECT INDEX:]");
        final Long userId = userService.currentUser.getId();
        final Project project = findByIndex(inputIndexCheckFormat(), userId);
        if (project == null) {
            System.out.println("[FAIL]");
            logger.info("Fail. Project was not updated");
        } else {
            System.out.println("[PLEASE, ENTER PROJECT NAME:]");
            final String name = scanner.nextLine();
            System.out.println("[PLEASE, ENTER PROJECT DESCRIPTION:]");
            final String description = scanner.nextLine();
            update(project.getId(), name, description, userId);
            logger.info("Project was updated");
            System.out.println("[OK]");
        }
        return 0;
    }

    public int updateProjectById() throws ProjectNotFoundException {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[PLEASE, ENTER PROJECT ID:]");
        final Long userId = userService.currentUser.getId();
        final Project project = findById(inputIdCheckFormat(), userId);
        if (project == null) {
            System.out.println("[FAIL]");
            logger.info("Project was not updated");
        } else {
            System.out.println("[PLEASE, ENTER PROJECT NAME:]");
            final String name = scanner.nextLine();
            System.out.println("[PLEASE, ENTER PROJECT DESCRIPTION:]");
            final String description = scanner.nextLine();
            update(project.getId(), name, description, userId);
            logger.info("Project was updated");
            System.out.println("[OK]");
        }
        return 0;
    }

    public int clearProject() throws ProjectNotFoundException {
        System.out.println("[CLEAR PROJECT]");
        if (userService.currentUser == null) {
            clear();
            projectTaskService.clear();
        } else {
            for (Project project : findAllByUserId(userService.currentUser.getId())) {
                removeById(project.getId(), userService.currentUser.getId());
            }
        }
        logger.info("Projects were deleted.");
        System.out.println("Projects were deleted.");
        return 0;
    }

    public int removeProjectByIndex() throws TaskNotFoundException, ProjectNotFoundException {
        System.out.println("[CLEAR PROJECT BY INDEX]");
        System.out.println("ENTER PROJECT INDEX: ");
        final Project project = removeByIndex(inputIndexCheckFormat(), userService.currentUser.getId());
        if (project == null) {
            System.out.println("[FAIL]");
            logger.info("Project is not removed.");
        } else {
            final List<Task> tasks = projectTaskService.findAllByProjectId(project.getId());
            for (final Task task : tasks) {
                projectTaskService.removeTaskFromProject(project.getId(), task.getId());
            }
            System.out.println("[OK]");
        }
        return 0;
    }

    public int removeProjectByName() throws ProjectNotFoundException {
        System.out.println("[CLEAR PROJECT BY NAME]");
        System.out.println("ENTER PROJECT NAME: ");
        final String name = scanner.nextLine();
        final List<Project> projects = removeByName(name, userService.currentUser.getId());
        if (projects == null) {
            System.out.println("[FAIL]");
            logger.info("Project was not removed.");
        } else {
            for (Project project : projects) {
                final List<Task> tasks = projectTaskService.findAllByProjectId(project.getId());
                for (final Task task : tasks) {
                    projectTaskService.removeTaskFromProject(project.getId(), task.getId());
                }
            }
            logger.info("Project was removed.");
            System.out.println("[OK]");
        }
        return 0;
    }

    public int removeProjectById() throws ProjectNotFoundException {
        System.out.println("[CLEAR PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID: ");
        final Project project = removeById(inputIdCheckFormat(), userService.currentUser.getId());
        if (project == null) {
            System.out.println("[FAIL]");
            logger.info("Project was not removed.");
        } else {
            final List<Task> tasks = projectTaskService.findAllByProjectId(project.getId());
            for (final Task task : tasks) {
                projectTaskService.removeTaskFromProject(project.getId(), task.getId());
            }
            logger.info("Project was removed.");
            System.out.println("[OK]");
        }
        return 0;
    }

    public void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

    public int viewProjectByIndex() throws ProjectNotFoundException, TaskNotFoundException {
        System.out.println("ENTER PROJECT INDEX: ");
        final Project project = findByIndex(inputIndexCheckFormat(), userService.currentUser.getId());
        viewProject(project);
        return 0;
    }

    public int viewProjectByName() throws ProjectNotFoundException {
        System.out.println("ENTER PROJECT NAME: ");
        final List<Project> projects = findByName(scanner.nextLine(), userService.currentUser.getId());
        viewProjects(projects);
        return 0;
    }

    public int viewProjectById() throws ProjectNotFoundException {
        System.out.println("ENTER PROJECT ID: ");
        final Project project = findById(inputIdCheckFormat(), userService.currentUser.getId());
        viewProject(project);
        return 0;
    }

    public int listProject() {
        System.out.println("[LIST PROJECT]");
        int index = 1;
        List<Project> projectList;
        if (userService.currentUser == null) {
            projectList = findAll();
        } else {
            projectList = findAllByUserId(userService.currentUser.getId());
        }
        viewProjects(projectList);
        return 0;
    }

    public void viewProjects(final List<Project> projects) {
        if (projects == null || projects.isEmpty()) {
            System.out.println("[FAIL]");
            logger.info("PROJECTS ARE NOT FOUND.");
            return;
        }
        int index = 1;
        projects.sort(Comparator.comparing(Project::getName));
        for (final Project project : projects) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
            index++;
        }
        System.out.println("[OK]");
    }

    public int saveJSON(final String  fileName){
        if (fileName == null) return -1;
        try {
            projectRepository.saveJSON(fileName);
        } catch (IOException e) {
            return sayErrorInFiles(e.getMessage());
        }
        System.out.println("[OK]");
        return 0;
    }

    public int saveXML(final String fileName){
        if (fileName == null) return -1;
        try {
            projectRepository.saveXML(fileName);
        } catch (IOException e) {
            return sayErrorInFiles(e.getMessage());
        }
        System.out.println("[OK]");
        return 0;
    }

    public int uploadFromJSON(final String  fileName){
        if (fileName == null) return -1;
        try {
            projectRepository. uploadJSON(fileName, Project.class);
        } catch (IOException e) {
            return sayErrorInFiles(e.getMessage());
        }
        System.out.println("[OK]");
        return 0;
    }

    public int uploadFromXML(final String  fileName){
        if (fileName == null) return -1;
        try {
            projectRepository. uploadXML(fileName, Project.class);
        } catch (IOException e) {
            return sayErrorInFiles(e.getMessage());
        }
        System.out.println("[OK]");
        return 0;
    }

}
