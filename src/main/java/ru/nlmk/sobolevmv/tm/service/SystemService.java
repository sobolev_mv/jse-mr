package ru.nlmk.sobolevmv.tm.service;

import java.util.ArrayDeque;
import java.util.Deque;

public class SystemService extends AbstractService {

    private static SystemService instance = null;

    public Deque<String> history = new ArrayDeque<>();

    public int historyLimit = 10;

    private SystemService() {
    }

    public static SystemService getInstance(){
        if (instance == null )  instance = new SystemService();
        return instance;
    }

    public int displayHistory(){
        if (history == null || history.isEmpty()) {
            System.out.println("[TASKS ARE NOT FOUND]");
            return -1;
        }
        int index = 1;
        for (final String command : history) {
            System.out.println(index + ". " + command);
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    public void addCommandToHistory(String command){
        if(command == null) return;
        history.add(command);
        if (history.size() > historyLimit){
            history.remove();
        }

    }

    public int displayError() {
        System.out.println("Error! Unknown program argument...");
        return -1;
    }

    public int displayExit() {
        System.out.println("Terminate console application...");
        return 0;
    }

    public void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    public int displayHelp() {
        System.out.println("log-on - Log on.");
        System.out.println("log-off - Log off.");
        System.out.println("user-info - User information.");
        System.out.println("user-update - Update user information.");
        System.out.println("user-update-password - Update current user password.");
        System.out.println("version - Display application version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of commands.");
        System.out.println("exit - Terminate console application.");
        System.out.println("history - History of commands.");
        System.out.println();
        System.out.println("project-create - Create project.");
        System.out.println("project-clear - Clear list of projects.");
        System.out.println("project-list - Display list of projects.");
        System.out.println("project-view-by-index - Display project by index.");
        System.out.println("project-view-by-id - Display project by id.");
        System.out.println("project-view-by-name - Display project by name.");
        System.out.println("project-remove-by-id - Remove project by id.");
        System.out.println("project-remove-by-name - Remove project by name.");
        System.out.println("project-remove-by-index - Remove project by index.");
        System.out.println("project-update-by-index - Update project by index.");
        System.out.println("project-update-by-id - Update project by id.");
        System.out.println();
        System.out.println("task-create - Create task.");
        System.out.println("task-clear - Clear list of tasks.");
        System.out.println("task-list - Display list of tasks.");
        System.out.println("task-view-by-index - Display task by index.");
        System.out.println("task-view-by-id - Display task by id.");
        System.out.println("task-view-by-name - Display task by name.");
        System.out.println("task-remove-by-id - Remove task by id.");
        System.out.println("task-remove-by-name - Remove task by name.");
        System.out.println("task-remove-by-index - Remove task by index.");
        System.out.println("task-update-by-index - Update task by index.");
        System.out.println("task-update-by-id - Update task by id.");
        System.out.println("task-list-by-project-id - Display task list by project id.");
        System.out.println("task-add-to-project-by-ids - Add task to project by ids.");
        System.out.println("task-remove-from-project-by-ids - Remove task from project by ids.");
        System.out.println();
        System.out.println("user-create - Create user.");
        System.out.println("user-clear - Clear list of users.");
        System.out.println("user-list - Display list of users.");
        System.out.println("user-view-by-login - Display user.");
        System.out.println("user-remove-by-login - Remove user bu login.");
        System.out.println("user-update-by-login - Update user by login.");
        return 0;
    }

    public int displayVersion() {
        System.out.println("1.0.0");
        return 0;
    }

    public int displayAbout() {
        System.out.println("Sobolev Mikhail");
        System.out.println("sobolev_mv@nlmk.com");
        return 0;
    }

}
