package ru.nlmk.sobolevmv.tm.service;

import ru.nlmk.sobolevmv.tm.entity.Project;
import ru.nlmk.sobolevmv.tm.entity.Task;
import ru.nlmk.sobolevmv.tm.exception.ProjectNotFoundException;
import ru.nlmk.sobolevmv.tm.exception.TaskNotFoundException;
import ru.nlmk.sobolevmv.tm.repository.ProjectRepository;
import ru.nlmk.sobolevmv.tm.repository.TaskRepository;

import java.util.List;

public class ProjectTaskService extends AbstractService  {

    private final ProjectRepository projectRepository;

    private final TaskRepository taskRepository;

    private static ProjectTaskService instance = null;

    private ProjectTaskService() {
        this.taskRepository = TaskRepository.getInstance();
        this.projectRepository = ProjectRepository.getInstance();
    }

    public static ProjectTaskService getInstance(){
        if (instance == null )  instance = new ProjectTaskService();
        return instance;
    }

    public Task removeTaskFromProject(final Long projectId, final Long taskId) {
        final Task task = taskRepository.findByProjectIdAndId(projectId, taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    public Task addTaskToProject(final Long projectId, final Long taskId, final Long userId) throws ProjectNotFoundException, TaskNotFoundException {
        final Project project = projectRepository.findById(projectId,userId);
        if (project == null) return null;
        final Task task = taskRepository.findById(taskId,userId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    public List<Task> findAllByProjectId(Long projectId) {
        return taskRepository.findAllByProjectId(projectId);
    }

    public void clear() {
        projectRepository.clear();
        taskRepository.clear();
    }

}
