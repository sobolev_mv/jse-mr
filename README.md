## Операционные системы
Windows, Mac, Linux

## Програмное обеспечение
Java 11.0.7, Apache Maven 3.6.1

## Сборка проекта
```bash
mvn install
```

## Разработчик
Соболев М.В. (sobolev_mv@nlmk.com)

## Запуск приложения
```bash
java -jar ./task-manager.jar
```
Пример запуска приложения с теринальной командой:

```
java -jar task-manager-1.0.0.jar help
```

Пример пример клонирования проекта по протоколу SSH:

```
git clone git@gitlab.com:sobolev_mv/jse-05
```

#### 6. Ссылка на резервную копию проекта

```
https://github.com/sobolev-mv/jse
```